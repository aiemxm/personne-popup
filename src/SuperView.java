
public interface SuperView  {
	public void afficher(String s);
	public void afficher(int s);
	public int saisirInt();
	public int saisirInt(int min , int max);
	public String saisieString(String message);
	public boolean saisirBoolean(String message);
}
