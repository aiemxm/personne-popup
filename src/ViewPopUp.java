import javax.swing.*;

public class ViewPopUp implements SuperView {
    private JFrame frame;

    public ViewPopUp() {
        this.frame = new JFrame();

    }

    @Override
    public void afficher(String s) {

        JOptionPane.showMessageDialog(frame, s);

    }

    @Override
    public void afficher(int s) {

    }

    @Override
    public int saisirInt() {
        boolean b = false;
        Integer i = 0;

        while (b == false) {
            try {
                i = Integer.valueOf(JOptionPane.showInputDialog(frame, "ton age"));
                b = true;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(frame, "Erreur, saisir un nombre");
            }
        }

        return i;

    }

    @Override
    public int saisirInt(int min, int max) {
        boolean b = false;
        Integer i = 0;

        while (b == false) {
            try {
                i = Integer.valueOf(JOptionPane.showInputDialog(frame, "Quel est ton age ?"));
                if (i >= min && i <= max){
                b = true;
                } else {
                    JOptionPane.showMessageDialog(frame, "erreur, votre age doit être compris entre" + min + "et" + max);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(frame, "Erreur, saisir un nombre");
            }
        }

        return i;
    }

    @Override
    public String saisieString(String message) {
        String reponse = JOptionPane.showInputDialog(frame, message);
        return reponse;
    }

    @Override
    public boolean saisirBoolean(String message) {
        int i = JOptionPane.showConfirmDialog(frame, message);
        boolean b = false;
        if (i == 0){
            b = true;
        }else if (i == 1){
            b = false;
        }

        return b;
    }


}
