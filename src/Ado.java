
public class Ado extends Mineur {
	private boolean portable;

	public Ado() {

	}

	public Ado(String nom, String prenom, int age, String ecole, boolean portable) {
		// TODO Auto-generated constructor stub
		super(nom, prenom, age, ecole);
		this.portable = portable;
		
	}

	public boolean isPortable() {
		return portable;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + (this.portable ? "tu as un portable " : "tu n'as pas de portable") + "\n";	}

	public void setPortable(boolean portable) {
		this.portable = portable;
	}
	
	public String conduireScooter() {
		return "j'ai un scooter";
	}
}
