
public class Enfant extends Mineur{
	private boolean velo;

	public Enfant() {

	}

	public Enfant(String nom, String prenom, int age, String ecole, boolean velo) {
		super(nom,prenom,age,ecole);
		this.velo=velo;
	}

	public boolean isVelo() {
		return velo;
	}

	public void setVelo(boolean velo) {
		this.velo = velo;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + (velo ? "A velo a Paris tu depasses les taxis" : "un kilomètre à pieds, ça use les souliers");
	}
	
	public String jouer() {
		return "je joue";
	}
}
