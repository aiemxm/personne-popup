
public class Mineur extends Personne {
	private String ecole;

	public Mineur() {

	}

	public Mineur(String nom, String prenom, int age, String ecole) {
		super(nom,prenom,age);
		this.ecole=ecole;
	}

	public String getEcole() {
		return ecole;
	}

	public void setEcole(String ecole) {
		this.ecole = ecole;
	}

	@Override
	public String toString() {
		return super.toString() + "\n" + "Ecole " + this.ecole + "\n";
	}
	
	public String grandir() {
		return "je grandis";
	}
}
