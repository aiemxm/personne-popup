
public class Majeur extends Personne {

	@Override
	public String toString() {
		return super.toString() +((vote) ? "tu as le droit de vote" : "tu n'as pas le droit de vote");
	}

	private boolean vote;

	public Majeur() {

	}
	public Majeur(String nom, String prenom, int age, boolean vote) {
		super(nom, prenom, age);
		this.vote= vote;
	}
	public boolean isVote() {
		return vote;
	}

	public void setVote(boolean vote) {
		this.vote = vote;
	}
	
	public String travailler() {
		return "je travaille";
	}

}
