
import java.util.Scanner;
public class View implements SuperView {
	private Scanner sc;
	public View() {
		this.sc = new Scanner(System.in);
	}

	public void afficher(String s) {
		System.out.println(s);
	}
	public void afficher(int s) {
		System.out.println(s);
	}
	public int saisirInt() {
		int i=0;
		boolean b = false;
		while(b==false) {
			this.afficher("Saisir un Entier");
			if(this.sc.hasNextInt()) {
				i = this.sc.nextInt();
				b=true;
			}else {
				this.afficher("[Erreur] Saisir un entier ");
				this.sc.nextLine();
			}
		}
		return i;
	}
	public int saisirInt(int min , int max) {
		int i=0;
		boolean b = false;
		while(b==false) {
			this.afficher("Saisir un Entier entre "+min+" et "+max);
			
			if(this.sc.hasNextInt()) {
				i = this.sc.nextInt();
				if(i>min&&i<max) {
					b=true;
				}
			}else {
				this.afficher("[Erreur] Saisir un entier ");
				this.sc.nextLine();
			}
		}
		return i;
	}
	public String saisieString(String message) {
		String s="";
		this.afficher(message);
		s=this.sc.next();
		return s;
	}
	public boolean saisirBoolean(String message) {
		this.afficher(message+"(o/n)");
		boolean rep=false;
		boolean b=false;
		String s= "";
		while(b==false) {
			s=this.saisieString("");
			if(s.equals("o")) {
				rep=true;
				b=true;
			}else if(s.equals("n")) {
				rep=false;
				b=true;
			}else {
				this.afficher("erreur");
				this.afficher(message+"(o/n)");
			}
		}
		return rep;
	}
}
